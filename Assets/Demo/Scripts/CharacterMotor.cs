using UnityEngine;
using System.Collections;
 
[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (CapsuleCollider))]
public class CharacterMotor : MonoBehaviour {
 
	public float speed = 1.0f;
	public float maxSpeedChange = 1.0f;
	public float jumpHeight = 1.0f;
	public float airMovement = 0.1f;
	public float wallSlide = -1.0f;
	public float minNormalDist = 0.5f;
	public float jumpCancelRate = 0.5f;
	public float wallJumpForce = 2.0f;
	
	[HideInInspector]
	public bool shouldJump = false;
	[HideInInspector]
	public bool cancelJump = false;
	[HideInInspector]
	public Vector3 targetVelocity = Vector3.zero;
	
	bool jumpCanceled = false;
	bool onWall = false;
	bool grounded = false;
 	Vector3 contactNorm = Vector3.zero;
	
	void Awake () {
	    rigidbody.freezeRotation = true;
	}
 
	public bool IsGrounded () {
		return grounded;
	}
	
	public bool IsOnWall () {
		return onWall && !grounded;	
	}
	
	public bool CanJump () {
		return onWall || grounded;	
	}
	
	public bool InAir () {
		return !onWall && !grounded;	
	}
	
	public Vector3 ContactNorm () {
		return contactNorm;	
	}
	
	void FixedUpdate () {
        Vector3 velocity = rigidbody.velocity;
        Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, -maxSpeedChange, maxSpeedChange);
        velocityChange.y = 0;
 
		if (InAir()) {
			velocityChange = velocityChange * airMovement;
			if (cancelJump && !jumpCanceled && rigidbody.velocity.y > 0) {
				jumpCanceled = true;
				velocityChange.y = -rigidbody.velocity.y * jumpCancelRate;
			}
		}
		else if (shouldJump) {
			if (IsOnWall())
				rigidbody.velocity = new Vector3(velocity.x, CalculateJumpSpeed(), velocity.z) + contactNorm * wallJumpForce;
			else
	            rigidbody.velocity = new Vector3(velocity.x, CalculateJumpSpeed(), velocity.z);
			print("here");
		}
		else if (IsOnWall()) {
			Vector3 slide = Vector3.up * Vector3.Dot(Vector3.down, contactNorm) * wallSlide;
			velocityChange += slide;	
		}
		
        rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);

		grounded = false;
		onWall = false;
	}
	
	void OnCollisionStay (Collision collision) {
		foreach (ContactPoint cp in collision.contacts) {
			contactNorm = cp.normal;
			float normDist = Vector3.Dot(Vector3.up, contactNorm);
			if (normDist > minNormalDist) {
				grounded = true;
			}
			else if (normDist <= minNormalDist && normDist >= 0) {
				onWall = true;
			}
			jumpCanceled = false;
		}
	}
 
	float CalculateJumpSpeed () {
	    return Mathf.Sqrt(2 * jumpHeight * -Physics.gravity.y);
	}
}