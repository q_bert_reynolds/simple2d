using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterMotor))]
public class PlayerMovementController : MonoBehaviour {
	
	public float turnThreshold = 0.1f;
	public float buttonResponseTime = 0.2f;
	public float dropFromWallThreshold = -0.2f;
	
	CharacterMotor motor;
	
	public string[] buttonNames = new string[4] {"Jump", "Fire1", "Fire2", "Fire3"};
	Dictionary<string, float> buttonLastPressedAt = new Dictionary<string, float>();
	Dictionary<string, float> buttonLastReleasedAt = new Dictionary<string, float>();
	
	void Awake () {
		motor = GetComponent<CharacterMotor>();
		foreach (string buttonName in buttonNames) {
			buttonLastPressedAt.Add(buttonName, Mathf.NegativeInfinity);
			buttonLastReleasedAt.Add(buttonName, Mathf.NegativeInfinity);
		}
	}
	
	Vector2 GetMovement () {
		return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
	}
	
	void Update () {
		GetInput();
		GetDirection();
	}
	
	void GetInput () {
		Vector2 movement = GetMovement();
		float x = movement.x;
		float y = movement.y;
		
		List<string> buttons = new List<string>(buttonLastPressedAt.Keys);
		foreach (string button in buttons) {
			if (Input.GetButtonDown(button)) 
				buttonLastPressedAt[button] = Time.time;
			if (Input.GetButtonUp(button)) 
				buttonLastReleasedAt[button] = Time.time;
		}
		
		// move
		if (!motor.IsOnWall() || y < dropFromWallThreshold) {
			motor.targetVelocity = Vector3.right * x * motor.speed;
		}
		else { 
			motor.targetVelocity = -motor.ContactNorm();
		}
		
		// jump
		motor.shouldJump = WasPressed("Jump");
		motor.cancelJump = !IsPressed("Jump");
	}
	
	bool WasPressed (string button) {
		return (Time.time - buttonLastPressedAt[button] < buttonResponseTime);
	}
	
	bool WasReleased (string button) {
		return (Time.time - buttonLastReleasedAt[button] < buttonResponseTime);	
	}
	
	bool IsPressed (string button) {
		return Input.GetButton(button);
	}
		
	void GetDirection () {
		float x = GetMovement().x;
		
		Vector3 scale = transform.localScale;
		if (x > turnThreshold) scale.x = Mathf.Abs(scale.x);
		else if (x < -turnThreshold) scale.x = -Mathf.Abs(scale.x);
		transform.localScale = scale;
	}
}
