using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class SpriteInfo {
	public string name;
	public Vector2 frame;
	public Rect uvs;
	public bool rotated;
}

[Serializable]
public class SpriteSheet : ScriptableObject {
	
	public Texture2D sheetTexture;
	public Material material;
	public Texture2D[] sourceTextures;
	
	[SerializeField]
	public List<SpriteInfo> spriteInfoList;
	
	[SerializeField]
	private int _maxSheetSize = 2048;
	public int maxSheetSize {
		get { return _maxSheetSize; }
		set {
			int x = roundToNearestPowerOfTwo(value);
			x = (x > 4096)?4096:x;			
			x = (x < 1)?1:x;
			
			_maxSheetSize = x;
		}
	}
	
	public Vector2 sheetSize {
		get {
			if (!sheetTexture) return Vector2.zero;
			else return new Vector2(sheetTexture.width, sheetTexture.height);
		}
	}
		
	public string[] spriteNames {
		get { return spriteInfoList.Select((info) => { return info.name; }).ToArray(); }
	}
	
	public SpriteInfo SpriteInfo(string spriteName) {
		foreach (SpriteInfo info in spriteInfoList)
			if (info.name == spriteName) return info;	
		return null;
	}
	
	public int SpriteInfoIndexOf (SpriteInfo info) {
		for (int i = 0; i < spriteInfoList.Count; i++) {
			if (info.name == spriteInfoList[i].name) return i;
		}
		return -1;
	}
	
	public int SpriteInfoIndexOf (string spriteName) {
		for (int i = 0; i < spriteInfoList.Count; i++) {
			if (spriteName == spriteInfoList[i].name) return i;
		}
		return -1;
	}
	
	public string SpriteNameFromIndex (int index) {
		return spriteInfoList[index].name;	
	}
	
	int roundToNearestPowerOfTwo (int n) {
		n--;
		n |= n >> 1;
		n |= n >> 2;
		n |= n >> 4;
		n |= n >> 8;
		n |= n >> 16;
		n++;
		return n;
	}

	public void OnEnable () {
		hideFlags = 0;
		if (spriteInfoList == null) {
			spriteInfoList = new List<SpriteInfo>();	
		}
	}
}
