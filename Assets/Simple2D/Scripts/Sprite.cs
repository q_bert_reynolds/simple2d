using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[ExecuteInEditMode]
public class Sprite : MonoBehaviour {
	
	public Color color = Color.white;
	public SpriteSheet sheet;
	
	public float spriteID = -1;
	[SerializeField]
	private string _spriteName;
	public string spriteName {
		get { return _spriteName; }
		set {
			_spriteName = value;
			spriteID = (float)sheet.SpriteInfoIndexOf(_spriteName);
		}
	}
	
	public Vector3 anchor = Vector3.zero;
	Vector3 bakedScale = Vector2.one;
	
	void Update () {
		if (!sheet) return;
		if (spriteID < 0) {
			spriteID = (float)sheet.SpriteInfoIndexOf(_spriteName);
		}
		
		int i = Mathf.RoundToInt(spriteID);
		if (i != sheet.SpriteInfoIndexOf(_spriteName)) {
			_spriteName = sheet.SpriteNameFromIndex(i);
			UpdateMesh();
		}
	}
	
	public void BakeScale () {
		Vector3 s = transform.localScale;
		transform.localScale = Vector3.one;

		bakedScale.x *= s.x;
		bakedScale.y *= s.y;
		bakedScale.z *= s.z;
		
		UpdateMesh();
	}
	
	public void UpdateMesh () {
		SpriteInfo info = sheet.SpriteInfo(spriteName);

		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		mesh = new Mesh();
		
		Vector3 scaledAnchor = anchor;
		scaledAnchor.x *= bakedScale.x;
		scaledAnchor.y *= bakedScale.y;
		
		// verts
		float left = -info.frame.x * bakedScale.x / 2.0f;
		float right = left + info.frame.x * bakedScale.x;
		float bottom = -info.frame.y * bakedScale.y / 2.0f;
		float top = bottom + info.frame.y * bakedScale.y;		
		Vector3 a = new Vector3(left, top ,0) + scaledAnchor;
		Vector3 b = new Vector3(right, top, 0) + scaledAnchor;
		Vector3 c = new Vector3(right, bottom, 0) + scaledAnchor;
		Vector3 d = new Vector3(left, bottom, 0) + scaledAnchor;
		mesh.vertices = new Vector3[] {a, b, c, d};

		// UVs
		left = info.uvs.x;
		right = left + info.uvs.width;
		bottom = info.uvs.y;
		top = bottom + info.uvs.height;

		Vector2 w = new Vector2(left, top);
		Vector2 x = new Vector2(right, top);
		Vector2 y = new Vector2(right, bottom);
		Vector2 z = new Vector2(left, bottom);
		
		if (info.rotated)
			mesh.uv = new Vector2[] {x, y, z, w};
		else 
			mesh.uv = new Vector2[] {w, x, y, z};
		
		
		// tris
		mesh.triangles = new int[] {0, 1, 2, 0, 2, 3};
		
		// colors
		mesh.colors = new Color[4] {color, color, color, color};
		
		GetComponent<MeshFilter>().sharedMesh = mesh;
		
		if (!renderer.sharedMaterial) renderer.sharedMaterial = sheet.material;
		
		BoxCollider box = GetComponent<BoxCollider>();
		if (box != null) {
			box.center = -anchor;
			box.extents = new Vector3(info.frame.x, info.frame.y, 1) * 0.5f;			
		}
	}	
}
