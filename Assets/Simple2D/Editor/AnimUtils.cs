using UnityEngine;
using UnityEditor;
using System.Collections;

/*
    The tangentMode is an undocumented bit variable that works something like this:

    Smooth Editable: 00, Smooth Auto: 01, Linear: 10, Stepped: 11

    The constructed binary value of a tangent mode would be built like this:

        [right-side][left-side][null bit]

    Eg. 11101 or [11][10][1] - linear in tangent, stepped out tangent
        01111 or [01][11][1] - constant in tangent, smooth out tangent

    Notes:
       The 'Broken' behaviour is simply an editor trick that moves the tangents
       independently if they do not have the same value. If the tangents have
       exact corresponding slopes, the editor will treat them as not broken, there
       is no flag indicating this state.
*/


/// <summary>
/// Utilities for working with animation data in the editor.
/// </summary>
public class AnimUtils {

    public enum TangentMode {SmoothEditable, SmoothAuto, Linear, Stepped}
    public enum TangentDirection {In, Out}

    public static int GetTangentMode(TangentMode inMode, TangentMode outMode) {
        int mode = SetInTangent(0, inMode);
        mode = SetOutTangent(mode, outMode);
        return CleanTangentMode(mode);
    }

	public static int GetTangentMode(int currentMode, TangentMode mode, TangentDirection inOut) {
        int output = currentMode;
        if (inOut == TangentDirection.In) {
            output = SetInTangent(output, mode);
        } else {
            output = SetOutTangent(output, mode);
        }
        return CleanTangentMode(output);
    }

    /// <summary> Return the in TangentMode of the given keyframe </summary>
    public static TangentMode GetInTangentMode(Keyframe key) {
        return GetInTangentMode(key.tangentMode);
    }

    /// <summary> Return the TangentMode value of the in tangent for the given tangent mode </summary>
    public static TangentMode GetInTangentMode(int mode) {
        // clear out tangent
        mode &= -25;
        mode = mode >> 1;
        return (TangentMode)mode;
    }

    /// <summary> Return the out TangentMode of the given keyframe </summary>
    public static TangentMode GetOutTangentMode(Keyframe key) {
        return GetOutTangentMode(key.tangentMode);
    }

    /// <summary> Return the TangentMode value of the out tangent for the given tangent mode </summary>
    public static TangentMode GetOutTangentMode(int mode) {
        // clear in tangent
        mode &= -7;
        mode = mode >> 3;
        return (TangentMode)mode;
    }

    static int SetInTangent(int current, TangentMode mode) {
        return (current & -7) | ((int)mode << 1);
    }

    static int SetOutTangent(int current, TangentMode mode) {
        return (current & -25) | ((int)mode << 3);
    }

    static int CleanTangentMode(int mode) {
        // unity wants 0 for first bit if 0 or 10 
        if (mode != 10 && mode != 0)
            mode |= 1;
        else
            mode &= -2;
        return mode;
    }

    /// <summary>
    /// Return a new Keyframe with the given in tangent mode.
    /// Tangent slopes should be cleaned after setting tangent mode.
    /// </summary>
    public static Keyframe SetKeyInTangentMode(Keyframe key, TangentMode mode) {
        return SetKeyTangentMode(key, mode, key.OutTangentMode());
    }

    /// <summary>
    /// Return the given keyframe with the given out tangent mode.
    /// Tangent slopes should be cleaned after setting tangent mode.
    /// </summary>
    public static Keyframe SetKeyOutTangentMode(Keyframe key, TangentMode mode) {
        return SetKeyTangentMode(key, key.InTangentMode(), mode);
    }

    /// <summary>
    /// Return the given Keyframe with the given in and out tangent modes
    /// Tangent slopes should be cleaned after setting tangent mode.
    /// </summary>
    public static Keyframe SetKeyTangentMode(Keyframe key, TangentMode inMode, TangentMode outMode) {
        key.tangentMode = GetTangentMode(inMode, outMode);
        return key;
    }

    /// <summary>
    /// Adjust the slope of the key at the given index to match its tangent mode
    /// eg. infinity for stepped, aim at next key for linear
    /// Simply changing the tangent mode of a key will leave its slopes
    /// unchanged so this must be used to ensure the mode and slopes match.
    /// </summary>
    public static Keyframe CleanKeyTangentSlopes(Keyframe[] keys, int keyIndex) {
        if (keyIndex < 0 || keyIndex >= keys.Length)
            return new Keyframe();
        Keyframe key = keys[keyIndex];
        switch (key.InTangentMode()) {
            case TangentMode.Stepped:
                key.inTangent = Mathf.Infinity;
                break;
            case TangentMode.Linear:
                break;
        }
        switch (key.OutTangentMode()) {
            case TangentMode.Stepped:
                key.outTangent = Mathf.Infinity;
                break;
            case TangentMode.Linear:
                break;
        }
        return key;
    }
}


public static class AnimUtilsExtensions {

    public static AnimUtils.TangentMode InTangentMode(this Keyframe key) {
        return AnimUtils.GetInTangentMode(key);
    }

    public static AnimUtils.TangentMode OutTangentMode(this Keyframe key) {
        return AnimUtils.GetOutTangentMode(key);
    }

}



