using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(SpriteSheet))]
[CanEditMultipleObjects]
public class SpriteSheetEditor : Editor {
	
	public SpriteSheet[] sheets {
		get {
			SpriteSheet[] sheets = targets as SpriteSheet[];
			if (sheets == null) sheets = new SpriteSheet[1] {target as SpriteSheet};
			return sheets;
		}
	}
	
	[MenuItem("Assets/Create/Sprite Sheet")]
    public static void CreateAsset () {
		SpriteSheet sheet = ScriptableObject.CreateInstance<SpriteSheet>();
		
		string path = AssetDatabase.GetAssetPath (Selection.activeObject);
        if (path == "") path = "Assets";
        else if (Path.GetExtension (path) != "")
            path = Path.GetDirectoryName(AssetDatabase.GetAssetPath (Selection.activeObject));
        path = AssetDatabase.GenerateUniqueAssetPath(path + "/NewSpriteSheet.asset");

        AssetDatabase.CreateAsset (sheet, path);

        AssetDatabase.SaveAssets ();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = sheet;
	}
	
	public static Rect[] PackTextures(Texture2D texture, Texture2D[] textures, int width, int height, int maxSize) {        
	
	    if(width > maxSize && height > maxSize) return null;
	    if(width > maxSize || height > maxSize) { int temp = width; width = height; height = temp; }
	
	    MaxRectsBinPack bp = new MaxRectsBinPack(width, height, true);
	
	    Rect [] rects = new Rect[textures.Length];
	
	    for(int i = 0; i < textures.Length; i++) {
	        Texture2D tex = textures[i];
	        Rect rect = bp.Insert(tex.width, tex.height, MaxRectsBinPack.FreeRectChoiceHeuristic.RectBestAreaFit);
	        if(rect.width == 0 || rect.height == 0) {
	            return PackTextures(texture, textures, width * (width <= height ? 2 : 1), height * (height < width ? 2 : 1), maxSize);
	        }
	        rects[i] = rect;
	    }
		
		TextureImporter sheetImporter = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(texture.GetInstanceID())) as TextureImporter;
		sheetImporter.isReadable = true;
		sheetImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
		sheetImporter.maxTextureSize = maxSize;
		AssetDatabase.ImportAsset(sheetImporter.assetPath, ImportAssetOptions.ForceUpdate);
	
	    texture.Resize(width, height);
	    texture.SetPixels(new Color[width * height]);
	
	    for(int i = 0; i < textures.Length; i++) {
	        Texture2D tex = textures[i];
	
			Rect rect = rects[i];
	        Color[] colors = tex.GetPixels();
	
	        if(rect.width != tex.width) {
	            Color[] newColors = tex.GetPixels();
	
				for(int x = 0; x < rect.width; x++) {
	                for(int y = 0; y < rect.height; y++) {
	                    int prevIndex = ((int)rect.height - (y + 1)) + x * (int)tex.width;
	                    newColors[x + y * (int)rect.width] = colors[prevIndex];
	                }
	            }
	            
	            colors = newColors;
	        }
	        
	        texture.SetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height, colors);

			rects[i] = rect;
	    }
				
		string path = AssetDatabase.GetAssetPath(texture.GetInstanceID());
		byte[] bytes = texture.EncodeToPNG();
		File.WriteAllBytes(path, bytes);
		EditorUtility.SetDirty(texture);
		AssetDatabase.SaveAssets();
		AssetDatabase.ImportAsset(path);
	    return rects;
	}
	
	public void PackTextures () {
		foreach (SpriteSheet s in sheets) {
			string path = AssetDatabase.GetAssetPath (Selection.activeObject);
	        if (path == "") path = "Assets";
	        else if (Path.GetExtension (path) != "")
	            path = Path.GetDirectoryName(AssetDatabase.GetAssetPath (Selection.activeObject));
		        
			if (!s.sheetTexture) {
				Texture2D newTex = new Texture2D(1,1);
				
				string texturePath = AssetDatabase.GenerateUniqueAssetPath(path + "/" + s.name + ".png");
				
				byte[] bytes = newTex.EncodeToPNG();
				File.WriteAllBytes(texturePath, bytes);
				AssetDatabase.ImportAsset(texturePath);
				
				s.sheetTexture = AssetDatabase.LoadMainAssetAtPath(texturePath) as Texture2D;
			}
			
			if (!s.material) {
				Material newMat = new Material(Shader.Find("Mobile/Particles/Alpha Blended"));
				newMat.mainTexture = s.sheetTexture;
				string matPath = AssetDatabase.GenerateUniqueAssetPath(path + "/" + s.name + ".mat");
				AssetDatabase.CreateAsset(newMat, matPath);
				AssetDatabase.SaveAssets();
				s.material = AssetDatabase.LoadMainAssetAtPath(matPath) as Material;
			}
			
			foreach (Texture2D tex in s.sourceTextures) {
				TextureImporter importer = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(tex.GetInstanceID())) as TextureImporter;
				importer.isReadable = true;
				importer.npotScale = TextureImporterNPOTScale.None;
				importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
				AssetDatabase.ImportAsset(importer.assetPath);
			}
			
			Texture2D[] textures = TrimTextures(s.sourceTextures);
			
			Rect[] rects = PackTextures(s.sheetTexture, textures, 1, 1, s.maxSheetSize);
			for (int i = 0; i < textures.Length; i++) {
				Texture2D tex = textures[i];
				Rect rect = rects[i];

				SpriteInfo info = new SpriteInfo();
				
				info.frame = new Vector2 (tex.width, tex.height);
   				
				info.rotated = ((int)tex.width != (int)rect.width || (int)tex.height != (int)rect.height);
				
				rect.x /= s.sheetSize.x;
		        rect.y /= s.sheetSize.y;
		        rect.width /= s.sheetSize.x;
		        rect.height /= s.sheetSize.y;
				info.uvs = rect;
				
				info.name = s.sourceTextures[i].name;
				
				int index = s.SpriteInfoIndexOf(info);
				if (index == -1) s.spriteInfoList.Add(info);
				else s.spriteInfoList[index] = info;
			}			
			
			EditorUtility.SetDirty(s);
			AssetDatabase.SaveAssets();
		}
	}
	
	public Texture2D[] TrimTextures (Texture2D[] textures) {
		int l = textures.Length;
		Texture2D[] newTextures = new Texture2D[l];
		for (int i = 0; i < l; i++) {
			Texture2D tex = textures[i];
			Color[] pixels = tex.GetPixels();
			
			int left = tex.width;
			int right = 0;
			int top = 0;
			int bottom = tex.width;
			
			for (int x = 0; x < tex.width; x++) {
				for (int y = 0; y < tex.height; y++) {
					int index = y * tex.width + x;
					float alpha = pixels[index].a;
					if (alpha > 0) {
						if (x < left) left = x;
						else if (x > right) right = x;
						if (y < bottom) bottom = y;
						else if (y > top) top = y;
					}
				}
			}
			
			Texture2D newTex = new Texture2D(right - left, top - bottom);
			newTex.SetPixels(tex.GetPixels(left, bottom, right - left, top - bottom));
			newTextures[i] = newTex;
		}
		return newTextures;
	}
	
	public override void OnInspectorGUI () {	
		
		DrawDefaultInspector();
				
		if (GUILayout.Button("Pack Sheet"))
			PackTextures();
	}
}
